const mongoose = require('mongoose');

const messageSchema = new mongoose.Schema({
    to: {
        type: String,
        required: [true, 'Cannot be blank']
    },
    from: {
        type: String,
        required: [true, 'Cannot be blank']
    },
    message: {
        type: String,
        required: [true, 'Cannot be blank']
    },
    seen: {
        type: Boolean
    }
});

module.exports = mongoose.model('Message', messageSchema);