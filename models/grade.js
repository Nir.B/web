const mongoose = require('mongoose');

const gradeSchema = new mongoose.Schema({
    username: {
        type: String,
        required: [true, 'Username cannot be blank']
    },
    subject: {
        type: String,
        required: [true, 'Subject cannot be blank']
    },
    description: {
        type: String,
        required: [true, 'Description cannot be blank']
    },
    grade: {
        type: Number,
        required: [true, 'Grade cannot be blank']
    },
    seen: {
        type: Boolean
    }

});

module.exports = mongoose.model('Grade', gradeSchema);