const mongoose = require('mongoose');

const statsSchema = new mongoose.Schema({
    username: {
        type: String,
        required: [true, 'Cannot be blank']
    },
    last_login: {
        type: Date,
        required: [true, 'Cannot be blank']
    }
});

module.exports = mongoose.model('Stats', statsSchema);