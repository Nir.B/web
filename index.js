const express = require('express');
var visitLoader = require('express-visit-counter');
const mongoose = require('mongoose');
const mongoSanitize = require('express-mongo-sanitize');
const path = require('path');
const app = express();
const User = require('./models/user');
const Grade = require('./models/grade');
const Message = require('./models/message');
const Stats = require('./models/stats');
const bcrypt = require('bcrypt');
const session = require('express-session');
const port = 80;

var counter = 0;

mongoose.connect('mongodb://localhost:27017/website', { useNewUrlParser: true, useUnifiedTopology: true, useFindAndModify: false})
    .then(() => {
        console.log('Mongo connection open')
    })
    .catch(err => {
        console.log('Connection error')
    });

app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, '/views'));
app.use(express.static(path.join(__dirname, 'views')));
app.use(session({ secret: 'notagoodsecret', saveUninitialized: true, resave: true }));
app.use(express.urlencoded({ extended: true }));
app.use(visitLoader.initialize());
app.use(mongoSanitize());


const requireLogin = (req, res, next) => {
    if (!req.session.user_id) {
        return res.redirect('/login');
    }
    next();
}

const requireAdmin = async (req, res, next) => {
    const user_id = req.session.user_id;
    const user = await User.findOne({ _id: user_id });
    if (!user.admin) {
        return res.redirect('/home');
    }
    next();
}

app.get('/home', requireLogin, async (req, res) => {
    const user_id = req.session.user_id;
    const user =  await User.findOne({ _id: user_id });
    const grades = await Grade.find({ username: user.username });
    const messages = await Message.find({ to: user.username });
    var new_messages = 0;

    for(var i = 0; i < messages.length; i++) {
        if(messages[i].seen == false) {
            new_messages++;
        }
    }

    var new_grades = 0;
    if(!user.admin) {
        for(var i = 0; i < grades.length; i++) {
            if(grades[i].seen == false) {
                new_grades++;
            }
        }
        res.render('home', { admin: user.admin, new_grades, new_messages });
    }
    else {
        res.render('home', { admin: user.admin, new_grades, new_messages });
    }
});

app.get('/message', requireLogin, async (req, res) => {
    const user_id = req.session.user_id;
    const user = await User.findOne({ _id: user_id });
    const users = await User.find();
    const messages = await Message.find();

    res.render('message', { user, error: "", users, messages: messages, view: false });
});

app.post('/message', requireLogin, async (req, res) => {
    const user_id = req.session.user_id;
    const user = await User.findOne({ _id: user_id });
    const users = await User.find();
    const { username, message} = req.body;
    var flag = false;
    var error = "";

    if(!user || !message) {
        flag = true;
        error = "One of the fields are invalid";
    }
    else if(user.username == username) {
        flag = true;
        error = "You cant message yourself!";
    }
    else {
        const new_message = new Message({ from: user.username, to: username, message, seen: false });
        new_message.save();
    }
    
    const messages = await Message.find({ $or: [{ from: user.username }, { to: user.username }] });
    if(flag) {
        res.render('message', { user, error, users, messages: messages, view: false });
    }
    else {
        res.redirect('/message');
    }

});

app.post('/view_message', requireLogin ,async (req, res) => {
    const user_id = req.session.user_id;
    const user = await User.findOne({ _id: user_id });
    const users = await User.find();
    const message = await Message.find({_id: req.body.message});

    if(message[0].seen == false && message[0].to == user.username) {
        await Message.findOneAndUpdate({ _id: message[0]._id }, { seen: true });
    }

    res.render('message', { user, error: "", users, messages: message, view: true });
});

app.get('/admin', requireLogin, requireAdmin, async (req, res) => {
    const user_id = req.session.user_id;
    const user = await User.findOne({ _id: user_id });
    const count = await Stats.countDocuments();
    const visitCounter = visitLoader.Loader

    let visitors = await visitCounter.getCount();
    let visitorsHome = await visitCounter.getCount("/home");
    let visitorsGrades = await visitCounter.getCount("/grades");
    let visitorsMessage = await visitCounter.getCount("/message");

    res.render('admin', { admin: user.admin, count, visitors, visitorsHome, visitorsGrades, visitorsMessage, counter });
});

app.get('/admin_login', requireLogin, requireAdmin, async (req, res) => {
    const user_id = req.session.user_id;
    const user = await User.findOne({ _id: user_id });
    const users = await User.find();
    const login = await Stats.find();

    res.render('admin_login', { admin: user.admin, login, users });
});

app.post('/admin_login', requireLogin, requireAdmin, async (req, res) => {
    const user_id = req.session.user_id;
    const username = req.body.username;
    const user = await User.findOne({ _id: user_id });
    const users = await User.find();
    
    if (username == 'Select user') {
        res.redirect('/admin_login');
    }
    else {
        const login = await Stats.find({ username });
        res.render('admin_login', { admin: user.admin, login, users });
    }
});

app.get('/new_grade', requireLogin, requireAdmin, async (req, res) => {
    const users = await User.find({ admin: false });
    
    res.render('new_grade', { users , error: "" });
});

app.post('/new_grade', requireLogin, requireAdmin, async (req, res) => {
    const { username, subject, description, grade } = req.body;
    const user = await User.findOne({ username });
    const users = await User.find({ admin: false });
    
    if(!username || !subject || !description || !grade || !user || subject == "Select subject") {
        res.render('new_grade', { users, error: "One of the fields are invalid" });
    }
    else {
        const new_grade = new Grade({ username, subject, description, grade, seen: false });
        new_grade.save();
        res.redirect('/home');
    }
});

app.get('/grades', requireLogin, async (req, res) => {
    const user_id = req.session.user_id;
    const user = await User.findOne({ _id: user_id });
    const users = await User.find({ admin: false });
    
    if(user.admin == false) {
        const grades = await Grade.find({username: user.username});

        for(var i = 0; i < grades.length; i++) {
            if(grades[i].seen == false)
            {
                await Grade.findOneAndUpdate({ _id: grades[i]._id }, { seen: true });
            }
        }

        res.render('grades', { grades, admin: user.admin});
    }
    else {
        res.render('grades_admin', { grades: [], admin: user.admin, users, subject: false });
    }
});

app.post('/grades', requireLogin, async (req, res) => {
    const user_id = req.session.user_id;
    const user = await User.findOne({ _id: user_id });
    var grades;

    if(req.body.subject == "Select subject") {
        grades = await Grade.find({ username: user.username });
    }
    else {
        grades = await Grade.find({ username: user.username, subject: req.body.subject });
    }

    for (var i = 0; i < grades.length; i++) {
        if (grades[i].seen == false) {
            await Grade.findOneAndUpdate({ _id: grades[i]._id }, { seen: true });
        }
    }

    res.render('grades', { grades, admin: user.admin });
});

app.post('/grades_admin', requireLogin, requireAdmin, async (req, res) => {
    const users = await User.find({ admin: false });
    if(req.body.username != "Select user" && req.body.subject == "Select subject") {
        const user = await User.findOne({ username: req.body.username });

        if(user) {
            const grades = await Grade.find({ username: user.username });
            res.render('grades_admin', { grades, admin: true, users });
        }
        else {
            res.render('grades_admin', { grades: [], admin: true, users });
        }
    }
    else if (req.body.username == "Select user" && req.body.subject != "Select subject"){
        const grades = await Grade.find({ subject: req.body.subject });
        res.render('grades_admin', { grades, admin: true, users });
    }
    else {
        const user = await User.findOne({ username: req.body.username });

        if (user) {
            const grades = await Grade.find({ username: user.username, subject: req.body.subject });
            res.render('grades_admin', { grades, admin: true, users });
        }
        else {
            res.render('grades_admin', { grades: [], admin: true, users });
        }
    }
});
app.post('/delete_grade', requireLogin, requireAdmin, async (req, res) => {
    await Grade.deleteOne({ _id: req.body.grade });
    res.redirect('/home');
});

app.get('/register', (req, res) => {
    res.render('register', { error: "" })
});

app.post('/register', async (req, res) => {
    const { password, username, passwordC } = req.body;
    const check_user = await User.findOne({ username });
    if(!password || !username) {
        res.render('register', {error: "Username or Password are invalid"});
    }
    else if(password != passwordC) {
        res.render('register', { error: "The passwords are not the same" });
    }
    else if (check_user) {
        res.render('register', {error: "User exist"});
    }
    else {
        const hash = await bcrypt.hash(password, 12);
        const user = new User({
            username,
            password: hash,
            admin: false
        });
        user.save();
        res.redirect('/');
    }
});

app.get('/login', async (req, res) => {
    const user_id = req.session.user_id;
    const user = await User.findOne({ _id: user_id });
    if(!user) {
        res.render('login', { error: "" });
    }
    else {
        res.redirect('/home');
    }
});

app.post('/login', async (req, res) => {
    const { username, password } = req.body;

    const user = await User.findOne({username});

    if(!user) {
        res.render('login', { error: "Username or Password are incorrect" });
    }
    else {
        const validPassword = await bcrypt.compare(password, user.password);
        if(validPassword) {
            req.session.user_id = user._id;
            var d = new Date();

            const stats = new Stats({username, last_login: d});
            stats.save();
            counter++;
            res.redirect('/home');
        }
        else {
            res.render('login', { error: "Username or Password are incorrect" });
        }
    }
});

app.get('/logout', (req, res) => {
    req.session.user_id = null;
    counter--;
    res.redirect('/login');
});

app.get('/', (req, res) => {
    res.redirect('/login')
});

app.listen(port, () => {
    console.log('Listening on port ' + port);
});